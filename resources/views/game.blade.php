@extends('main')

@section('content')
    <div class="container mt-5">
        <div class="card text-center p-3">
            <h2>Eres el jugador: {{$player}}</h2>
            <hr>
            <div class="row">
                <div class="col-7">
                    @if($player == 1)
                        <input type="text" value="" placeholder="jugador {{$player}}" id="jugador1">
                        <input type="text" value="" placeholder="jugador {{$player}}" id="jugador2" disabled>
                    @else
                        <input type="text" value="" placeholder="jugador {{$player}}" id="jugador1" disabled>
                        <input type="text" value="" placeholder="jugador {{$player}}" id="jugador2">
                    @endif
                </div>
                <div class="col-auto">
                    <h3>Codigo: {{$code}} </h3>
                </div>
            </div>

            @if ($turno == $player)
                <div class="row bg-success bg-opacity-50">
                    <h3>Tu turno</h3>
                </div>
            @else
                <div class="row bg-danger bg-opacity-50">
                    <h3>Esperando al otro jugador</h3>
                </div>
            @endif

            <div class="game container" id="board">
                <div class="row firstRow w-75 mx-auto mt-3 justify-content-center">
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="0" value="">
                        <input id="l-0" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="1" value="">
                        <input id="l-1" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="2" value="">
                        <input id="l-2" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                </div>
                <div class="row firstRow w-75 mx-auto mt-3 justify-content-center">
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="3" value="">
                        <input id="l-3" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="4" value="">
                        <input id="l-4" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="5" value="">
                        <input id="l-5" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                </div>
                <div class="row firstRow w-75 mx-auto mt-3 justify-content-center">
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="6" value="">
                        <input id="l-6" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="7" value="">
                        <input id="l-7" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                    <button class="col-2 mx-2 btn btn-outline-dark" style="height: 100px;" id="8" value="">
                        <input id="l-8" value='' class="border-0 w-100 text-center" disabled>
                    </button>
                </div>
            </div>

            <hr>
            <button class="mx-auto w-25 btn btn-outline-secondary" id="restart">Reiniciar</button>

        </div>
    </div>
@endsection


{{----------------------------------------------------------------------------------------------------------------------}}


{{-- SCRIPT NECESARIO PARA CONECTAR CON FIREBASE Y FUNCIONAMIENTO --}}
@push('script')
    <script>
        //CREDENCIALES DE FIREBASE
        const firebaseConfig = {
            apiKey: "{{ config('services.firebase.apiKey') }}",
            authDomain: "{{ config('services.firebase.authDomain') }}",
            databaseURL: "{{ config('services.firebase.databaseURL') }}",
            projectId: "{{ config('services.firebase.projectId') }}",
            storageBucket: "{{ config('services.firebase.storageBucket') }}",
            messagingSenderId: "{{ config('services.firebase.messagingSenderId') }}",
            appId: "{{ config('services.firebase.appId') }}"
        };
        firebase.initializeApp(firebaseConfig);
        var database = firebase.firestore();

        //VARIABLES GLOBALES
        let player = '<?=$player?>';
        let code = '<?=$code?>';
        let turno = '<?=$turno?>';
        let URL = '<?=$_SERVER['HTTP_HOST']?>'

        //METODO RESTART PARA CAMBIAR JUGADOR A QUIEN CORRESPONDA Y VOLVER A EMPEZAR EL JUEGO
        $('#restart').on('click',function(){
            let initGame = 0;
            //PETICION GET FIREBASE
            firebase.database().ref('games/' + code).on('value', function(snapshot){
                var gameData = snapshot.val();
                if(gameData.initGame){
                    initGame = gameData.initGame;
                }else {
                    initGame = 1;
                }
            })
            if(initGame == 1){
                initGame = 2;
            }else {
                initGame =1;
            }
            let initCloudBoard = ['','','','','','','','','']

            //DATOS INICIALES DEL JUEGO SUBIDA A FIREBASE
            firebase.database().ref('games/' + code).set({
                board: initCloudBoard,
                turn: initGame,
                initGame: initGame,
                jugador1: document.getElementById('jugador1').value,
                jugador2:  document.getElementById('jugador2').value,
            })
            window.location.replace('/game/'+player+'/'+code+'/'+turno);
        })

        //ADD THE GAMECODE AND MOVE TO GAME
        $("#board button").on('click', function(){

            //VALIDACION DE TURNO VALIDO
            if(turno == player){
                let move = false;
                let cloudBoard = [];
                var space = $(this).prop('id')
                for (let i = 0; i < 9; i++) {
                    let tempValue = document.getElementById('l-'+i).value;

                    //VALIDACION DE NO SOBRE-ESCRITURA
                    if(space == i && tempValue == ''){
                        if(player == 1){
                            move = true;
                            cloudBoard.push('X');
                        }else {
                            move = true;
                            cloudBoard.push('O');
                        }
                    }else {
                        cloudBoard.push(tempValue);
                    }
                }

                if(move){
                    if(turno == 1){
                        turno = 2;
                    }else {
                        turno = 1;
                    }

                    firebase.database().ref('games/' + code).set({
                        board: cloudBoard,
                        turn: turno,
                        jugador1: document.getElementById('jugador1').value,
                        jugador2:  document.getElementById('jugador2').value,
                    })
                    window.location.replace('/game/'+player+'/'+code+'/'+turno);
                }                
            }
        })

        //CONSULTA Y CARGA DE CAMBIOS EN BASE DE DATOS
        firebase.database().ref('games/' + code).on('value', function(snapshot){
            var gameData = snapshot.val();
            if(gameData){
                for (let i = 0; i < gameData.board.length; i++) {
                    let tempRef = document.getElementById('l-'+i);
                    tempRef.value = gameData.board[i];
                }
                if(gameData.turn != turno){
                    window.location.replace('/game/'+player+'/'+code+'/'+gameData.turn);
                }

                if(gameData.jugador1){
                    document.getElementById('jugador1').value = gameData.jugador1;
                }

                if(gameData.jugador2){
                    document.getElementById('jugador2').value = gameData.jugador2;
                }

            }
        })

        //WIN CONDITION
        if(player == 1){
            firebase.database().ref('games/' + code).on('value', function(snapshot){
                var gameData = snapshot.val();
                let winner = '';
                if(gameData){

                    //VALIDACION CASOS HORIZONTALES
                    for (let i = 1; i < gameData.board.length; i+=3) {
                        if(gameData.board[i] == gameData.board[i-1] && gameData.board[i] == gameData.board[i+1]){
                            if(gameData.board[i] != ''){
                                winner = 'EL GANADOR ES: ' + gameData.board[i];
                            }
                        }
                    }

                    //VALIDACION CASOS VERTICALES
                    for (let i = 3; i < 6; i++) {
                        if(gameData.board[i] == gameData.board[i-3] && gameData.board[i] ==gameData.board[i+3]){
                            if(gameData.board[i] != ''){
                                winner = 'EL GANADOR ES: ' + gameData.board[i];
                            }   
                        }
                    }

                    //VALIDACION DIAGONAL PRINCIPAL
                    if(gameData.board[0] == gameData.board[4] && gameData.board[0] == gameData.board[8]){
                        if(gameData.board[0] != ''){
                            winner = 'EL GANADOR ES: ' + gameData.board[0];
                        }
                    }

                    //VALIDACION DIAGONAL INVERTIDA
                    if(gameData.board[2] == gameData.board[4] && gameData.board[2] == gameData.board[6]){
                        if(gameData.board[2] != ''){
                            winner = 'EL GANADOR ES: ' + gameData.board[2];
                        }
                    }

                    if(winner != ''){
                        window.location.replace('/game/save/'+code+'/'+ document.getElementById('jugador1').value +'/'+ document.getElementById('jugador1').value +'/'+ winner);
                        alert(winner);
                        document.getElementById('restart').click();
                    }else if(!gameData.board.includes('')){
                        window.location.replace('/game/save/'+code+'/'+ document.getElementById('jugador1').value +'/'+ document.getElementById('jugador1').value +'/EMPATE');
                        alert('EMPATE');
                        document.getElementById('restart').click();
                    }
                }
            })
        }
    </script>
@endpush